<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('status')
            ->withTimestamps();

        return $this->belongsToMany('App\User', 'book_user')
            ->withTimestamps();
    }



    protected $fillable = [
        'title', 'author', 'ISBN', 'edition', 'publication', 'copies', 'available'
    ];
}