<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Book;
use App\History;
use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user_id=Auth::check();
        $user=User::where('id',$user_id)->first();
        $year=date('Y');
        $month=date('m');
        $day=date('d');
        $data=['year'=>$year,'month'=>$month,'day'=>$day];
       // echo $month;
        return view('home',compact('data'))->withUser($user);
    }

    public function history()//$user_id)
    {
        $books=Book::all();
        $user = Auth::user()->id;
        $users=History::where('user_id',$user)->get();
        return view('user.history',compact('users'),compact('books'));
    }

    public function borrowed() {

        return view('user.borrowed');
    }

//    public function search() {
//
//        return view('user.search');
//    }
}
