<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class UserController extends Controller
{
     public function index(){
         $users = User::all();
         return view('/root/users.index')->withUsers($users);
         //return view('/home');
     }

     public function create() {
         return view('/root/users.create');
     }

     public function store(Request $request) {

         $this->validate($request, [
             'name' => 'required|alpha_num|unique:users|min:3|max:32',
             'email' => 'required|email|unique:users|max:32',
             'password' => 'required|min:6|max:32',
         ]);

         $data = $request->all();

         $user = User::create([
             'name' => $data['name'],
             'email' => $data['email'],
             'password' => bcrypt($data['password']),
         ]);

         return redirect()->route('users.show', $user);
     }

     public function show(User $user) {
         $users = User::all();
         return view('/root/users.show')->withUser($user);
         //return view('users.show', ['user' => $user, 'users' => $users])->withUser($user);
     }

     public function edit(User $user) {
         return view('/root/users.edit')->withUser($user);
     }

     public function update(Request $request, User $user) {

         $this->validate($request, [
             'name' => 'required|alpha_num|min:3|max:32',
             'email' => 'required|email|unique:users,email,'.$user->id.',id|max:32',
             'admin' => 'required|boolean',
         ]);

         $user->name = $request->name;
         $user->email = $request->email;
         $user->admin = $request->admin;
         $user->save();
         return redirect()->route('users.show', $user);
     }

     public function destroy(User $user) {
         $user->delete();
         return redirect()->route('users.index');
     }
}
