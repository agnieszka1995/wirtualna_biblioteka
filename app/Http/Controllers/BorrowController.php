<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Book;
use App\History;


class BorrowController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::all();
        return view('root/borrow.index')->withUsers($users);
    }

    public function create()
    {

    }


    public function show($id)
    {
        $user=User::where('id',$id)->first();
        $books=Book::all();
        //$book->pivot->status;
       return view('root/borrow.show')->withUser($user);//->withBooks($books)->withPivot('status');
       //return view('root/borrow.show')->withUser($book)->withPivot('status');
    }

    public function edit($id_user, $id_book)
    {
        $book=Book::where('id', $id_book)->first();
        $pivotTable = $book->users()->find($id_user)->pivot;
        $status = $pivotTable->status;
        $user=User::where('id',$id_user)->first();

        if (!$status) {     // status 0 - ksiazka w koszyku
            $pivotTable->status = 1;
            $pivotTable->update();
            $user->borrowed=$user->borrowed+1;
            $user->save();
            return redirect()->route('borrow.index');
        }
        else {      // status 1 - ksiazka wypozyczona

            $book->available = $book->available + 1;
            $user->borrowed=$user->borrowed-1;
            $book->save();
            $user->save();

            $history=History::create([
                'user_id' => $id_user,
                'book_id' => $id_book,
            ]);
            $a=History::where('id',$history->id)->first();
            $a->user_id=$id_user;
            $a->book_id=$id_book;
            $a->created_at=$pivotTable->updated_at;
            $a->save();

            $history->save();
            $pivotTable->delete();
            return redirect()->route('borrow.index');

            //CONFIG->DATABASE.PHP ->MYSQL 'strict' => true,

        }
    }

    public function store(Request $request,$id_user,$id_book)
    {

    }
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
