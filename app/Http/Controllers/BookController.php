<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BookController extends Controller
{
    public function index(){
        $books = Book::all();
        return view('/root/books.index')->withBooks($books);
    }

    public function create() {
        return view('/root/books.create');
    }

    public function store(Request $request) {

        $this->validate($request, [
            'title' => 'required|min:3|max:255',
            'author' => 'required|min:3|max:255',
            'ISBN' => 'required|unique:books|max:32',
            'edition' => 'required|min:3|max:255',
            'publication' => 'required|min:3|max:255',
            'copies' => 'required|numeric|min:0|max:2147483647',
            //'available' => 'required|numeric',
        ]);

        $data = $request->all();

        $book = Book::create([
            'title' => $data['title'],
            'author' => $data['author'],
            'ISBN' => $data['ISBN'],
            'edition' => $data['edition'],
            'publication' => $data['publication'],
            'copies' => $data['copies'],
            'available' => $data['copies'],
        ]);

        return redirect()->route('books.show', $book);
    }

    public function show(Book $book) {
        $books = Book::all();
        return view('/root/books.show')->withBook($book);
    }

    public function edit(Book $book) {
        return view('/root/books.edit')->withBook($book);
    }

    public function update(Request $request, Book $book) {

        $this->validate($request, [
            'title' => 'required|min:3|max:255',
            'author' => 'required|min:3|max:255',
            'ISBN' => 'required|unique:books,ISBN,'.$book->id.',id|max:32',
            'edition' => 'required|min:3|max:255',
            'publication' => 'required|min:3|max:255',
            'copies' => 'required|numeric|min:0|max:2147483647',
            'available' => 'required|numeric|min:0|max:2147483647',
        ]);

        $book->title = $request->title;
        $book->author = $request->author;
        $book->ISBN = $request->ISBN;
        $book->edition = $request->edition;
        $book->publication = $request->publication;
        $book->copies = intval($request->copies);
        if (intval($request->available) > intval($request->copies))
            $book->available = intval($request->copies);
        else
            $book->available = intval($request->available);
        $book->save();
        return redirect()->route('books.show', $book);
    }

    public function destroy(Book $book) {
        $book->delete();
        return redirect()->route('books.index');
    }
}
