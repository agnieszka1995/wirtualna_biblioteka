<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
class CatalogController extends Controller
{
    public function index()
    {
        $catalogs = Book::paginate(50);

        return view('catalog.index', compact('catalogs'));

    }

    public function search(Request $request )
    {
        $search=$request->input('search');
        $catalogs= Book::where('title', 'like', '%'.$search.'%')
            ->orWhere('author', 'like', '%'.$search.'%')
            ->orWhere('ISBN', 'like', '%'.$search.'%')
            ->orWhere('edition', 'like', '%'.$search.'%')
            ->orWhere('publication', 'like', '%'.$search.'%')->orderBy('title')->paginate(50);
        return view('catalog.index',['catalogs' => $catalogs]);
    }
    public function store()
    {
        //
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
