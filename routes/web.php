<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\User;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(array('middleware' => ['admin']), function() {
    Route::get('/admin', 'AdminController@index');
    Route::resource('/admin/users', 'UserController');
    Route::resource('/admin/books', 'BookController');
    Route::get('/admin/borrow/{user}/edit/{book}','BorrowController@edit');
    Route::resource('/admin/borrow','BorrowController');
});

Route::get('/home', 'HomeController@index');
Route::resource('catalog', 'CatalogController@index');
Route::resource('basket', 'BasketController');



Route::get('/user/history', 'HomeController@history');
Route::get('/user/borrowed', 'HomeController@borrowed');
//Route::get('/user/search', 'HomeController@search');
Route::post('search', 'CatalogController@search');

/*
Route::get('/users', function () {
    $users = User::all();
    return $users;
});
Route::get('/users/{user}', function ($user) {
    $user = User::findOrFail($user);
    return $user;
});
*/

