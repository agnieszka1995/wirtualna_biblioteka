### Setup ###

na początek wyczyścić bazę danych "test" (localhost:8080, test:test123)

```
git clone https://bitbucket.org/agnieszka1995/wirtualna_biblioteka.git
cd wirtualna_biblioteka
chmod u+x setup.sh
./setup.sh
```

w razie chęci zmiany wyglądu strony (css, do np. zmiany szerokości kolumn w blade nie jest to potrzebne) należy dodatkowo uruchomić

```
npm install
```

i po wprowadzeniu zmian

```
gulp
```

(nie dodaję tego do setup.sh, bo nie zawsze jest to potrzebne a zajmuje sporo czasu (i miejsca na dysku))


### Użytkownicy do testów: ###
* root@ro.ot:123
* qw@er.ty:456