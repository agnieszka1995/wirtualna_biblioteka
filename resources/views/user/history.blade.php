@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Historia wypożyczeń</div>

                    <div class="panel-body">
                        <table class='table table-hover'>
                            <th>Tytuł</th><th>Autor</th><th>Data wypożyczenia</th><th>Data zwrotu</th>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $books->find($user->book_id)->title    }}</td>
                                    <td>{{ $books->find($user->book_id)->author    }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>{{ $user->updated_at }}</td>
                                </tr>
                            @endforeach
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection