@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Wypożyczone książki</div>

                    <div class="panel-body">
                        <table class='table table-hover'>

                            @if(Auth::user()->borrowed==0)
                               <h3>Brak wypożyczonych książek</h3>
                            @else
                                <th>Tytuł</th><th>Autor</th><th>Data wypożyczenia</th>
                                @foreach(Auth::user()->books as $book)
                                    @if(($book->pivot->status)==1)
                                        <tr>
                                         <td>{{ $book->title }}</td>
                                         <td>{{ $book->author }}</td>
                                         <td>{{ $book->updated_at }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection