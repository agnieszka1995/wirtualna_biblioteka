@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Wypożyczenia</div>

                    <div class="panel-body">
                        <table class='table table-hover'>
                            <th>ID</th><th>Nazwa użytkownika</th><th>E-mail</th><th>Do wypożyczenia</th></th><th>Wypożyczone</th><th>Zarządzaj</th>
                            @foreach ($users as $user)
                                @foreach ($user->books as $book)
                                    @if ($book->pivot->status == 0 | $book->pivot->status == 1)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if (!$book->pivot->status)
                                                    {{ $book->title }}</br>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($book->pivot->status)
                                                    {{ $book->title }}</br>
                                                @endif
                                            </td>
                                            <td><a href="{{ route('borrow.show',$user)}}">Edytuj</a></td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection