@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Edytuj książkę</div>

                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" action="{{route('books.update', $book)}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Tytuł:</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="title" value="{{ $book->title }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-md-4 control-label">Autor:</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="author" value="{{ $book->author }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="ISBN" class="col-md-4 control-label">ISBN:</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="ISBN" value="{{ $book->ISBN }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="edition" class="col-md-4 control-label">Wydanie:</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="edition" value="{{ $book->edition }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="publication" class="col-md-4 control-label">Wydawnictwo:</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="publication" value="{{ $book->publication }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="copies" class="col-md-4 control-label">Liczba kopii:</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="copies" value="{{ $book->copies }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="available" class="col-md-4 control-label">Dostępnych kopii:</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="available" value="{{ $book->available }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <input type="submit" value="Zapisz zmiany" class="btn btn-primary">
                                </div>
                            </div>
                        </form>
                        {{--<form class="form-horizontal" action="{{route('books.destroy',$book)}}" method="post" onSubmit="if(!confirm('Jesteś pewien, że chcesz usunąć książkę?')){return false;}">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-8 col-md-offset-4">--}}
                                    {{--<input type="submit" value="Usuń książkę" class="btn btn-primary">--}}
                                    {{--{{csrf_field()}}--}}
                                    {{--{{method_field('DELETE')}}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                        <br><a href="{{ route('books.index') }}">Powrót</a>
                    </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection