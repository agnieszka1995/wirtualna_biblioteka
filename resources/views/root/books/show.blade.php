@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Informacje o książce</div>

                    <div class="panel-body">
                        <h3>{{$book->author}} - {{$book->title}}</h3>
                        <b>Tytuł</b>: {{$book->title}}<br>
                        <b>Autor</b>: {{$book->author}}<br>
                        <b>ISBN</b>: {{$book->ISBN}}<br>
                        <b>Wydanie</b>: {{$book->edition}}<br>
                        <b>Wydawnictwo</b>: {{$book->publication}}<br>
                        <b>Liczba kopii</b>: {{$book->copies}}<br>
                        <b>Dostępnych kopii</b>: {{$book->available}}<br>
                        <hr>
                        <form action="{{route('books.destroy',$book)}}" method="post" onSubmit="if(!confirm('Jesteś pewien, że chcesz usunąć książkę?')){return false;}">
                            <input type="submit" value="Usuń książkę">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                        </form>
                        <br><a href="{{ route('books.index') }}">Powrót</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection