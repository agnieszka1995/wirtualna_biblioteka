@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Książki</div>

                    <div class="panel-body">
                        <table class='table table-hover' wrap>
                            <th>ID</th><th>Tytuł</th><th>Autor</th><th>ISBN</th><th>Wydanie</th><th>Wydawnictwo</th><th>Kopie</th><th></th>
                            @foreach ($books as $book)
                                <tr>
                                    <td>{{ $book->id }}</td>
                                    <td>{{ $book->title }}</td>
                                    <td>{{ $book->author }}</td>
                                    <td>{{ $book->ISBN }}</td>
                                    <td>{{ $book->edition }}</td>
                                    <td>{{ $book->publication }}</td>
                                    <td>{{ $book->available }}/{{ $book->copies }}</td>
                                    <td><a href="{{ route('books.edit', $book) }}">Edytuj</a></td>
                                </tr>
                            @endforeach
                        </table>
                        <hr>
                        <a href="{{ route('books.create') }}">Dodaj nową książkę</a><br>
                        <a href="/admin">Powrót</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection