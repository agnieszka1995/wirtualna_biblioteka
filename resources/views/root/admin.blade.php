@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Admin</div>

                    <div class="panel-body">
                        <a href="/admin/users">Zarządzaj użytkownikami</a><br>
                        <a href="/admin/books">Zarządzaj książkami</a>
                        <a> Koszyk</a><br>
                    </div>

                    </div>
            </div>
        </div>
    </div>

@endsection