@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Informacje o użytkowniku</div>

                    <div class="panel-body">
                        <h3>{{$user->name}}</h3>
                        <table class='table table-hover'>
                            <th>ID</th><th>Nazwa użytkownika</th><th>E-mail</th><th>Info</th><th></th>
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if ($user->admin)
                                        admin
                                    @else
                                        user
                                    @endif
                                </td>
                                <td><a href="{{ route('users.edit', $user) }}">Edytuj</a></td>
                            </tr>
                        </table>
                        <hr>
                        <form action="{{route('users.destroy',$user)}}" method="post" onSubmit="if(!confirm('Jesteś pewien, że chcesz usunąć tego użytkownika?')){return false;}">
                            <input type="submit" value="Usuń użytkownika">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                        </form>
                        <br><a href="{{ route('users.index') }}">Powrót</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection