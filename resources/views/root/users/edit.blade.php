@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Edytuj użytkownika</div>

                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" action="{{route('users.update', $user)}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Nazwa:</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="name" value="{{ $user->name }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">E-mail:</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="email" value="{{ $user->email }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="admin"  class="col-md-4 control-label">Rola:</label>
                                <div class="col-md-6">
                                    <select name="admin" class="col-md-4 control-label">
                                        <option value="0" @php if ($user->admin == 0) echo "selected"; @endphp>user</option>
                                        <option value="1" @php if ($user->admin == 1) echo "selected"; @endphp>admin</option>
                                    </select>
                                    {{--<input type="text" name="admin" value="{{ $user->admin }}"><br>--}}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <input type="submit" value="Zapisz zmiany" class="btn btn-primary">
                                </div>
                            </div>
                        </form>

                        <form class="form-horizontal" action="{{route('users.destroy',$user)}}" method="post" onSubmit="if(!confirm('Jesteś pewien, że chcesz usunąć użytkownika?')){return false;}">
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <input type="submit" value="Usuń użytkownika" class="btn btn-primary">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                </div>
                            </div>
                        </form>
                        <br><a href="{{ route('users.index') }}">Powrót</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection