@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Użytkownicy</div>

                    <div class="panel-body">
                        <table class='table table-hover'>
                            <th>ID</th><th>Nazwa użytkownika</th><th>E-mail</th><th>Info</th><th></th>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if ($user->admin)
                                            admin
                                        @else
                                            user
                                        @endif
                                    </td>
                                    <td><a href="{{ route('users.edit', $user) }}">Edytuj</a></td>
                                </tr>
                            @endforeach
                        </table>
                        <hr>
                        <a href="{{ route('users.create') }}">Dodaj nowego użytkownika</a><br>
                        <a href="/admin">Powrót</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection