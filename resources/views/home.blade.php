@extends('layouts.app')

@section('content')

    <div class="container">

        {{--Opcja 1 - powiadomienie pojawia się tylko wtedy, kiedy jest potrzebne.--}}
        {{--Na przykład:    alert-warning kiedy do oddania książki zostało (0,7> dni,--}}
        {{--alert-danger kiedy książka nie została oddana na czas.--}}
        {{--W tej wersji powiadomienie można zamknąć (do czasu odświeżenia strony).--}}

        <div class="row">
            <div class="col-md-7  col-md-offset-1">

                <div class="alert alert-warning" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Nowa funkcjonalność,</strong> sprawdź historię wypożyczeń
                </div>

                <div class="panel panel-default">

                    <div class="panel-heading">Witaj, {{Auth::user()->name}}</div>

                    <div class="panel-body">
                        <strong> Zarezerwuj książki, udaj się do biblioteki i wypożycz!</strong><br><br>
                        Zachęcamy do zapoznania się z nowym regulaminem, który obowiązuje od dnia 21.01.2017r. Najistotniejsza zmiana, wprowadzona na prośbę wielu czytelników dotyczy terminu zwrotu książek. Przedłużyliśmy ją do 30 dni. Nowy regulamin jest dostępny w bibliotece oraz na stronie miasta, www.mojemiastokrakow.pl
                        <br><br><br>
                        <strong>Relacja ze spotkania z Augustynem Rostowickim</strong><br><br>

                        W dniu 15.01.2017 w naszej bibliotece miało miejsce spotkanie z fenomenalnym pisarzem, Augustynem. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-3">

                    <div class="panel panel-default">
                        <div  class="panel-heading">Powiadomienia</div>
                        <div class="panel-body">
                            <h3 style="font-size:20px"><b>  Zwroty</b></h3>
                            <table class="table table-hover">
                                <div>
                                    <th>Tytuł</th><th>Data zwrotu</th>
                                </div>
                                @foreach($user->books as $book)
                                    @if($book->pivot->status==1)
                                    <tr>
                                        @if($book->pivot->updated_at->month==01)
                                            @if($book->pivot->updated_at->day==30)
                                                @if(31-$book->pivot->updated_at->day+$data['day']>=23 || $data['day']==1 )
                                                    <td>{{$book->title}}</td>
                                                    <td>1-2-{{$book->pivot->updated_at->year}}</td>
                                                @endif
                                            @elseif($book->pivot->updated_at->day==31)
                                                @if(31-$book->pivot->updated_at->day+$data['day']>=23 || $data['day']==1 || $data['day']==2)
                                                    <td>{{$book->title}}</td>
                                                    <td>2-3-{{$book->pivot->updated_at->year}}</td>
                                                @endif
                                            @elseif($book->pivot->updated_at->day+23<=31 &&  $book->pivot->updated_at->month==$data['month'] )
                                                @if($book->pivot->updated_at->day+23<=$data['day'])
                                                    <td>{{$book->title}}</td>
                                                    @if($book->pivot->updated_at->day==1)
                                                        <td>31-1-{{$data['year']}}</td>
                                                    @else
                                                        <td>{{$book->pivot->updated_at->day-1}}-2-{{$book->pivot->updated_at->year}}</td>
                                                    @endif
                                                 @endif
                                            @elseif($book->pivot->updated_at->day+23>31 && $data['month']==$book->pivot->updated_at->month+1)
                                                @if(31-$book->pivot->updated_at->day+$data['day']>=23)
                                                    <td>{{$book->title}}</td>
                                                    <td>{{$book->pivot->updated_at->day-1}}-2-{{$book->pivot->updated_at->year}}</td>
                                                @endif
                                            @endif

                                        @elseif( $book->pivot->updated_at->month==03 || $book->pivot->updated_at->month==05 || $book->pivot->updated_at->month==07 ||$book->pivot->updated_at->month==8 || $book->pivot->updated_at->month==10)
                                            @if($book->pivot->updated_at->day+23<=31 &&  $book->pivot->updated_at->month==$data['month'])
                                                @if($book->pivot->updated_at->day+23<=$data['day'])
                                                    <td>{{$book->title}}</td>
                                                    @if($book->pivot->updated_at->day==1)
                                                        <td>31-{{$book->pivot->updated_at->month}}-{{$data['year']}}</td>
                                                     @else
                                                        <td>{{$book->pivot->updated_at->day-1}}-{{$book->pivot->updated_at->month+1}}-{{$book->pivot->updated_at->year}}</td>
                                                     @endif
                                                @endif
                                            @elseif($book->pivot->updated_at->day+23>31 &&  $book->pivot->updated_at->month+1==$data['month'])
                                                @if(31-$book->pivot->updated_at->day+$data['day']>=23)
                                                    <td>{{$book->title}}</td>
                                                    <td>{{$book->pivot->updated_at->day-1}}-{{$book->pivot->updated_at->month+1}}-{{$book->pivot->updated_at->year}}</td>
                                                @endif
                                            @endif

                                        @elseif($book->pivot->updated_at->month==12)
                                            @if($book->pivot->updated_at->day+23<=31 &&  $book->pivot->updated_at->month==$data['month'])
                                                @if($book->pivot->updated_at->day+23<=$data['day'])
                                                    <td>{{$book->title}}</td>
                                                    @if($book->pivot->updated_at->day==1)
                                                        <td>31-12-{{$data['year']}}</td>
                                                    @else
                                                        <td>{{$book->pivot->updated_at->day-1}}-1-{{$book->pivot->updated_at->year+1}}</td>
                                                    @endif
                                                @endif
                                            @elseif($book->pivot->updated_at->day+23>31 &&  $book->pivot->updated_at->month+1==$data['month'])
                                                @if(31-$book->pivot->updated_at->day+$data['day']>=23)
                                                    <td>{{$book->title}}</td>
                                                    <td>{{$book->pivot->updated_at->day-1}}-1-{{$book->pivot->updated_at->year+1}}</td>
                                                @endif
                                            @endif

                                        @elseif($book->pivot->updated_at->month==04 || $book->pivot->updated_at->month==06 || $book->pivot->updated_at->month==9 || $book->pivot->updated_at->month==11 )
                                            @if($book->pivot->updated_at->day+23<=30 &&  $book->pivot->updated_at->month==$data['month'])
                                                @if($book->pivot->updated_at->day+23<=$data['day'])
                                                    <td>{{$book->title}}</td>
                                                    <td>{{$book->pivot->updated_at->day}}-{{$book->pivot->updated_at->month+1}}-{{$book->pivot->updated_at->year}}</td>
                                                @endif
                                            @elseif($book->pivot->updated_at->day+23>30 &&  $book->pivot->updated_at->month+1==$data['month'])
                                                @if(30-$book->pivot->updated_at->day+$data['day']>=23)
                                                    <td>{{$book->title}}</td>
                                                    <td>{{$book->pivot->updated_at->day}}-{{$book->pivot->updated_at->month+1}}-{{$book->pivot->updated_at->year}}</td>
                                                @endif
                                            @endif


                                        @else
                                            @if($book->pivot->updated_at->day+23<=28 &&  $book->pivot->updated_at->month==$data['month'])
                                                @if($book->pivot->updated_at->day+23<=$data['day'])
                                                    <td>{{$book->title}}</td>
                                                    <td>{{$book->pivot->updated_at->day+2}}-3-{{$book->pivot->updated_at->year}}</td>
                                                @endif
                                            @elseif($book->pivot->updated_at->day+23>28 &&  $book->pivot->updated_at->month+1==$data['month'])
                                                @if(28-$book->pivot->updated_at->day+$data['day']>=23)
                                                    <td>{{$book->title}}</td>
                                                    <td>{{$book->pivot->updated_at->day+2}}-3-{{$book->pivot->updated_at->year}}</td>
                                                @endif
                                            @endif

                                        @endif
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>


        {{--Ewentualnie opcja 3 - powiadomienie w dropdown menu (wtedy zmiany w layouts/app.blade.php)--}}

    </div>

@endsection
