<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(BooksTableSeeder::class);
        $this->call(BookUserTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(HistoriesTableSeeder::class);
    }
}
