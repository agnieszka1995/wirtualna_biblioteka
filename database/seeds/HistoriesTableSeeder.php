<?php

use Illuminate\Database\Seeder;
use App\History;
class HistoriesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('histories')->delete();

        History::create([
            'user_id' => 1,
            'book_id' => 2,
        ]);
        History::create([
            'user_id' => 1,
            'book_id' => 3,
        ]);
        History::create([
            'user_id' => 2,
            'book_id' => 2,
        ]);
        History::create([
            'user_id' => 2,
            'book_id' => 1,
        ]);
        History::create([
            'user_id' => 2,
            'book_id' => 3,
        ]);
        History::create([
            'user_id' => 1,
            'book_id' => 1,
        ]);
    }
}
