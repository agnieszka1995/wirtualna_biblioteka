<?php

use Illuminate\Database\Seeder;
use App\Book;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->delete();

        $faker= \Faker\Factory::create();

        for ($i=0; $i <1000; $i++) {

            $book = new Book();
            $book->title = $faker->sentence(5);
            $book->author = $faker->name;
            $book->ISBN = $faker->isbn13;
            $book->edition = $faker->company;
            $book->publication = $faker->company;
            $book->copies = $faker->randomDigit;
            $book->available = $book->copies;
            $book->save();
        }

        /**Book::create([
            'title' => 'Zielona mila',
            'author'=>'Stephen King',
            'ISBN'=>'111-854-4511',
            'edition'=>'Wyd. II',
            'publication'=>'Express Publishing',
            'copies'=>2,
            'available'=>2
        ]);

        Book::create([
            'title' => 'Podziemny Krąg',
            'author'=>'Chuck Palahniuk',
            'ISBN'=>'111-784-921',
            'edition'=>'Wyd. V',
            'publication'=>'Media Rodzina',
            'copies'=>3,
            'available'=>3
        ]);

        Book::create([
            'title'=>'Kuchnia śródziemnomorska',
            'author'=>'Mrowiec Justyna',
            'ISBN'=>'154-854-4511',
            'edition'=>'Wyd. I',
            'publication'=>'Wydawnictwo Buchmann',
            'copies'=>2,
            'available'=>2
        ]);
        **/


    }
}
